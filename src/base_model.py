'''
Created on Oct 8, 2020

from https://www.kaggle.com/bochen0909/kubi-pytorch-moa/edit

@author: ec2-user
'''
from utils import seed_everything, log_loss_multi
import torch
from data import Data
from iterstrat.ml_stratifiers import MultilabelStratifiedKFold
import  numpy as np 
from torch import nn
import torch.nn.functional as F
import torch.optim as optim
from tqdm import tqdm 
import pandas as pd 
 
DEVICE = ('cuda' if torch.cuda.is_available() else 'cpu')


class MoADataset:

    def __init__(self, features, targets, targets_nonscored):
        self.features = features
        self.targets = targets
        self.targets_nonscored = targets_nonscored
        
    def __len__(self):
        return (self.features.shape[0])
    
    def __getitem__(self, idx):
        dct = {
            'x' : torch.tensor(self.features[idx, :], dtype=torch.float),
            'y' : torch.tensor(self.targets[idx, :], dtype=torch.float),
            'y2': torch.tensor(self.targets_nonscored[idx, :], dtype=torch.float)
        }
        return dct

    
class TestDataset:

    def __init__(self, features):
        self.features = features
        
    def __len__(self):
        return (self.features.shape[0])
    
    def __getitem__(self, idx):
        dct = {
            'x' : torch.tensor(self.features[idx, :], dtype=torch.float)
        }
        return dct

    
def train_fn(model, optimizer, scheduler, loss_fn, dataloader, device, use_non_scored):
    model.train()
    final_loss1 = 0
    final_loss2 = 0
    
    for data in dataloader:
        optimizer.zero_grad()
        inputs, targets, targets2 = data['x'].to(device), data['y'].to(device), data['y2'].to(device)

        outputs, outputs2 = model(inputs)
        loss1, loss2 = loss_fn(outputs, targets), loss_fn(outputs2, targets2)
        if use_non_scored: 
            loss = loss1 + loss2
        else:
            loss = loss1
        loss.backward()
        optimizer.step()
        scheduler.step()
        
        final_loss1 += loss1.item()
        final_loss2 += loss2.item()
        
    final_loss1 /= len(dataloader)
    final_loss2 /= len(dataloader)
    
    return final_loss1, final_loss2


def valid_fn(model, loss_fn, dataloader, device):
    model.eval()
    final_loss1 = 0
    final_loss2 = 0
    valid_preds = []
    
    for data in dataloader:
        inputs, targets, targets2 = data['x'].to(device), data['y'].to(device), data['y2'].to(device)
        outputs, outputs2 = model(inputs)
        loss1, loss2 = loss_fn(outputs, targets), loss_fn(outputs2, targets2)
                
        final_loss1 += loss1.item()
        final_loss2 += loss2.item()
        valid_preds.append(outputs.sigmoid().detach().cpu().numpy())
        
    final_loss1 /= len(dataloader)
    final_loss2 /= len(dataloader)
    valid_preds = np.concatenate(valid_preds)
    
    return final_loss1, final_loss2, valid_preds


def inference_fn(model, dataloader, device):
    model.eval()
    preds = []
    
    for data in dataloader:
        inputs = data['x'].to(device)

        with torch.no_grad():
            outputs, _ = model(inputs)
        
        preds.append(outputs.sigmoid().detach().cpu().numpy())
        
    preds = np.concatenate(preds)
    
    return preds

   
class Model(nn.Module):

    def __init__(self, num_features, num_targets, num_targets2, hidden_size):
        super(Model, self).__init__()
        self.batch_norm1 = nn.BatchNorm1d(num_features)
        self.dropout1 = nn.Dropout(0.2)
        self.dense1 = nn.utils.weight_norm(nn.Linear(num_features, hidden_size))
        
        self.batch_norm2 = nn.BatchNorm1d(hidden_size)
        self.dropout2 = nn.Dropout(0.3)
        self.dense2 = nn.utils.weight_norm(nn.Linear(hidden_size, hidden_size))
        
        self.batch_norm3 = nn.BatchNorm1d(hidden_size)
        self.dropout3 = nn.Dropout(0.25)
        self.dense3 = nn.utils.weight_norm(nn.Linear(hidden_size, num_targets))
        self.dense32 = nn.utils.weight_norm(nn.Linear(hidden_size, num_targets2))
    
    def forward(self, x):
        x = self.batch_norm1(x)
        x = self.dropout1(x)
        x = F.relu(self.dense1(x))
        
        x = self.batch_norm2(x)
        x = self.dropout2(x)
        x = F.relu(self.dense2(x))
        
        x = self.batch_norm3(x)
        x = self.dropout3(x)
        x1 = self.dense3(x)
        x2 = self.dense32(x)
        
        return x1, x2       


def run_training(args, data:Data, fold, tv_idx):
    t_idx, v_idx = tv_idx
    test_ = data.test
    train = data.train
    target = data.train_target_df
    target_nonscored = data.train_targets_nonscored_df
    
    train_df = train.iloc[t_idx]
    valid_df = train.iloc[v_idx]
    
    x_train, y_train = train_df.values, target.loc[train_df.index].values
    x_valid, y_valid = valid_df.values, target.loc[valid_df.index].values
    y_train_nonscored = target_nonscored.loc[train_df.index].values
    y_valid_nonscored = target_nonscored.loc[valid_df.index].values         
    
    train_dataset = MoADataset(x_train, y_train, y_train_nonscored)
    valid_dataset = MoADataset(x_valid, y_valid, y_valid_nonscored)
    trainloader = torch.utils.data.DataLoader(train_dataset, batch_size=args['BATCH_SIZE'], shuffle=True)
    validloader = torch.utils.data.DataLoader(valid_dataset, batch_size=args['BATCH_SIZE'], shuffle=False)
    
    num_features = train_df.shape[1]
    num_targets = target.shape[1]
    num_targets2 = target_nonscored.shape[1]
    model = Model(
        num_features=num_features,
        num_targets=num_targets,
        num_targets2=num_targets2,
        hidden_size=args['hidden_size'],
    )
    
    model.to(DEVICE)
    
    optimizer = torch.optim.Adam(model.parameters(), lr=args['LEARNING_RATE'], weight_decay=args['WEIGHT_DECAY'])
    scheduler = optim.lr_scheduler.OneCycleLR(optimizer=optimizer, pct_start=0.1, div_factor=1e3,
                                              max_lr=1e-2, epochs=args['EPOCHS'], steps_per_epoch=len(trainloader))
    
    loss_fn = nn.BCEWithLogitsLoss()
    
    early_stopping_steps = args['EARLY_STOPPING_STEPS']
    early_step = 0
    
    oof = np.zeros((len(train), target.shape[1]))
    best_loss = np.inf
    
    for epoch in tqdm(range(args['EPOCHS'])):
        if args['use_non_scored'] and epoch < args['EPOCHS'] // 3:
            use_non_scored = True 
        else:
            use_non_scored = False
        train_loss, train_loss2 = train_fn(model, optimizer, scheduler, loss_fn, trainloader, DEVICE, use_non_scored)
        if use_non_scored:
            total_loss = train_loss + train_loss2
        else:
            total_loss = train_loss
            
        print(f"\nFOLD: {fold}, EPOCH: {epoch}, train_loss: {train_loss:.6f}, total_loss: {total_loss:.6f}")
        valid_loss, valid_loss2, valid_preds = valid_fn(model, loss_fn, validloader, DEVICE)
        if use_non_scored:
            total_valid_loss = valid_loss + valid_loss2
        else:
            total_valid_loss = valid_loss

        print(f"FOLD: {fold}, EPOCH: {epoch}, valid_loss: {valid_loss:.6f}, total_valid_loss: {total_valid_loss:.6f}")
        
        if valid_loss < best_loss:
            
            best_loss = valid_loss
            oof[v_idx] = valid_preds
            torch.save(model.state_dict(), f"{args['model_path']}/FOLD{fold}.pth")
        
        elif(args['EARLY_STOP'] == True):
            
            early_step += 1
            if (early_step >= early_stopping_steps):
                break
    
    #--------------------- PREDICTION---------------------
    x_test = test_.values
    testdataset = TestDataset(x_test)
    testloader = torch.utils.data.DataLoader(testdataset, batch_size=args['BATCH_SIZE'], shuffle=False)
    
    model = Model(
        num_features=num_features,
        num_targets=num_targets,
        num_targets2=num_targets2,
        hidden_size=args['hidden_size'],
    )
    
    model.load_state_dict(torch.load(f"{args['model_path']}/FOLD{fold}.pth"))
    model.to(DEVICE)
    
    predictions = inference_fn(model, testloader, DEVICE)
    
    return oof, predictions


def run_k_fold(args, data:Data, NFOLDS):
    train = data.train
    target = data.train_target_df
    if 1:
        target12 = target 
    else:
        if args['use_non_scored']:
            target12 = pd.concat([target, data.train_targets_nonscored_df], axis=1)
        else:
            target12 = target 
    test = data.test
    oof = np.zeros((len(train), target.shape[1]))
    predictions = np.zeros((len(test), target.shape[1]))
    
    mskf = MultilabelStratifiedKFold(n_splits=NFOLDS)
    for f, (t_idx, v_idx) in enumerate(mskf.split(X=train, y=target12)):
        oof_, pred_ = run_training(args, data, f, (t_idx, v_idx))
        predictions += pred_ / NFOLDS
        oof += oof_
        
    return oof, predictions


def run(_args, NFOLDS, data:Data, seed):
    print("running with seed " + str(seed))
    seed_everything(seed)
    args = dict(
        EPOCHS=25,
        BATCH_SIZE=128,
        LEARNING_RATE=1e-3,
        WEIGHT_DECAY=1e-5,
        EARLY_STOPPING_STEPS=10,
        EARLY_STOP=True,
        hidden_size=2048,
        test_pred=True,
        use_non_scored=False,
        model_path='/tmp'
        )
    args.update(_args)
    print(args)    
    oof, predictions = run_k_fold(args, data, NFOLDS)
    loss = log_loss_multi(data.train_target_df.values, oof)
    print("seed={}, n_fold={}, loss={}".format(seed, NFOLDS, loss))
    return oof, predictions

    
def main(args, NFOLDS, seeds):
    data = Data()
    oofs = 0
    predictions = 0
    for seed in seeds:
        a, b = run(args, NFOLDS, data, seed)
        oofs = oofs + a
        predictions = predictions + b
    
    oofs /= len(seeds) 
    predictions /= len(seeds)
    print(predictions.shape)
    loss = log_loss_multi(data.train_target_df.values, oofs)
    print("seed={}, n_fold={}, loss={}".format(str(seeds), NFOLDS, loss))
    
    sub = data.sample_submission * 0
    test_pred = pd.DataFrame(predictions, index=data.test.index, columns=sub.columns)
    sub.loc[test_pred.index] = test_pred
    print (sub.shape, test_pred.shape)
    sub.to_csv("submission.csv")


if __name__ == '__main__':
    EPOCHS = 2; NFOLDS = 2
    # EPOCHS=25; NFOLDS=7
    args = {'EPOCHS':EPOCHS, 'use_non_scored':True}
    main(args, NFOLDS, [123, 124])

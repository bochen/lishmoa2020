'''
Created on Oct 10, 2020

@author: ec2-user
'''

import os

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torchvision.transforms as transforms

import pyro
import pyro.distributions as dist
from pyro.infer import SVI, Trace_ELBO
from pyro.optim import Adam
from data import Data
import utils

pyro.enable_validation(True)
pyro.distributions.enable_validation(False)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class Option:
    pass 


Option.n_layer = 1
Option.n_input_features = 872


class MoAVAEDataset:

    def __init__(self, features):
        self.features = features

    def __len__(self):
        return (self.features.shape[0])
    
    def __getitem__(self, idx):
        x = torch.tensor(self.features[idx, :], dtype=torch.float)
        dct = {
            'x' : x,
        }
        return dct


def setup_data_loaders(batch_size=128, use_cuda=False, use_all_data=False):
    data = Data(use_pca=False)
    columns = data.g_columns + data.c_columns
    test_ = data.test[columns]
    train_ = data.train[columns]
    train_test = pd.concat([train_, test_], axis=0)
    train_test = train_test.loc[np.random.permutation(train_test.index)]
    print('min/max', train_test.min().min(), train_test.max().max())
    k = train_test.shape[0] // 5
    if use_all_data:
        train_df = train_test
    else:
        train_df = train_test.iloc[k:]
    valid_df = train_test.iloc[:k]
    
    print('train.shape={}, valid.shape={}'.format(train_df.shape, valid_df.shape))
    train_set = MoAVAEDataset(train_df.values)
    valid_set = MoAVAEDataset(valid_df.values)

    kwargs = {'num_workers': 1, 'pin_memory': use_cuda}
    train_loader = torch.utils.data.DataLoader(dataset=train_set,
        batch_size=batch_size, shuffle=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(dataset=valid_set,
        batch_size=batch_size, shuffle=False, **kwargs)
    return train_loader, test_loader


class Decoder(nn.Module):

    def __init__(self, z_dim, hidden_dim):
        super().__init__()
        if 1:
            self.fc1 = nn.Linear(z_dim, hidden_dim)
            self.softplus1 = nn.Softplus()
            self.fc2 = nn.Linear(hidden_dim, hidden_dim)
            self.softplus2 = nn.Softplus()
            self.fco1 = nn.Linear(hidden_dim, Option.n_input_features)
            self.fco2 = nn.Linear(hidden_dim, Option.n_input_features)
        # setup the non-linearities
        
        self.sigmoid = nn.Sigmoid()

    def forward(self, z):
        hidden = self.softplus1(self.fc1(z))
        if Option.n_layer >= 2:
            hidden = self.softplus2(self.fc2(hidden))
        loc_img = self.sigmoid(self.fco1(hidden))
        scale_img = torch.exp(self.fco2(hidden))
        return loc_img, scale_img

class Encoder(nn.Module):

    def __init__(self, z_dim, hidden_dim):
        super().__init__()
        # setup the three linear transformations used
        if 1:
            self.fc1 = nn.Linear(Option.n_input_features, hidden_dim)
            self.softplus1 = nn.Softplus()
            self.fc2 = nn.Linear(hidden_dim, hidden_dim)
            self.softplus2 = nn.Softplus()
            self.fco1 = nn.Linear(hidden_dim, z_dim)
            self.fco2 = nn.Linear(hidden_dim, z_dim)            

    def forward(self, x):
        x = self.softplus1(self.fc1(x))
        if Option.n_layer >= 2:
            x = self.softplus2(self.fc2(x))
        
        hidden = x  # each of size batch_size x z_dim
        z_loc = self.fco1(hidden)
        z_scale = torch.exp(self.fco2(hidden))
        return z_loc, z_scale


class VAE(nn.Module):

    # by default our latent space is 50-dimensional
    # and we use 400 hidden units
    def __init__(self, z_dim=128, hidden_dim=512, use_cuda=False):
        super().__init__()
        # create the encoder and decoder networks
        self.encoder = Encoder(z_dim, hidden_dim)
        self.decoder = Decoder(z_dim, hidden_dim)

        if use_cuda:
            self.cuda()
        self.use_cuda = use_cuda
        self.z_dim = z_dim

    # define the model p(x|z)p(z)
    def model(self, x):
        # register PyTorch module `decoder` with Pyro
        pyro.module("decoder", self.decoder)
        x = x['x']
        
        with pyro.plate("data", x.shape[0]):
            z_loc = x.new_zeros(torch.Size((x.shape[0], self.z_dim)))
            z_scale = x.new_ones(torch.Size((x.shape[0], self.z_dim)))
            
            z = pyro.sample("latent", dist.Normal(z_loc, z_scale).to_event(1))
            loc_img, scale_img = self.decoder.forward(z)
            pyro.sample("obs", dist.Normal(loc_img, scale_img).to_event(1), obs=x)

    # define the guide (i.e. variational distribution) q(z|x)
    def guide(self, x):
        # register PyTorch module `encoder` with Pyro
        pyro.module("encoder", self.encoder)
        x = x['x']
        with pyro.plate("data", x.shape[0]):
            # use the encoder to get the parameters used to define q(z|x)
            z_loc, z_scale = self.encoder.forward(x)
            # sample the latent code z
            pyro.sample("latent", dist.Normal(z_loc, z_scale).to_event(1))

    # define a helper function for reconstructing images
    def reconstruct_img(self, x):
        # encode image x
        z_loc, z_scale = self.encoder(x)
        # sample in latent space
        z = dist.Normal(z_loc, z_scale).sample()
        # decode the image (note we don't sample in image space)
        loc_img = self.decoder(z)
        return loc_img

    
def train(svi, train_loader, use_cuda=False):
    # initialize loss accumulator
    epoch_loss = 0.
    
    
    for x in train_loader:
        # if on GPU put mini-batch into CUDA memory
        if use_cuda:
            x = x.cuda()
        # do ELBO gradient and accumulate loss
        epoch_loss += svi.step(x)

    # return epoch loss
    normalizer_train = len(train_loader.dataset)
    total_epoch_loss_train = epoch_loss / normalizer_train
    return total_epoch_loss_train


def evaluate(svi, test_loader, use_cuda=False):
    # initialize loss accumulator
    test_loss = 0.
    # compute the loss over the entire test set
    for x in test_loader:
        # if on GPU put mini-batch into CUDA memory
        if use_cuda:
            x = x.cuda()
        # compute ELBO estimate and accumulate loss
        test_loss += svi.evaluate_loss(x)
    normalizer_test = len(test_loader.dataset)
    total_epoch_loss_test = test_loss / normalizer_test
    return total_epoch_loss_test
    

# saves the model and optimizer states to disk
def save_checkpoint(model, filepath):
    torch.save(model.state_dict(), filepath)


# loads the model and optimizer states from disk
def load_checkpoint(model, filepath):
    assert os.path.exists(filepath)
    sd = torch.load(filepath)
    model.load_state_dict(sd)

        
def run():
    
    utils.seed_everything(123)
    
    LEARNING_RATE = 1.0e-3
    USE_CUDA = False
    
    # Run only for a single iteration for testing
    NUM_EPOCHS = 150
    TEST_FREQUENCY = 5
    
    train_loader, test_loader = setup_data_loaders(batch_size=256, use_cuda=USE_CUDA)

    pyro.clear_param_store()
    
    # setup the VAE
    vae = VAE(use_cuda=USE_CUDA)
    
    # setup the optimizer
    adam_args = {"lr": LEARNING_RATE}
    optimizer = Adam(adam_args)
    
    # setup the inference algorithm
    svi = SVI(vae.model, vae.guide, optimizer, loss=Trace_ELBO())
    
    train_elbo = []
    test_elbo = []
    # training loop
    for epoch in range(NUM_EPOCHS):
        total_epoch_loss_train = train(svi, train_loader, use_cuda=USE_CUDA)
        train_elbo.append(-total_epoch_loss_train)
        print("[epoch %03d] average training loss: %.4f" % (epoch, total_epoch_loss_train))
    
        if epoch % TEST_FREQUENCY == 0:
            # report test diagnostics
            total_epoch_loss_test = evaluate(svi, test_loader, use_cuda=USE_CUDA)
            test_elbo.append(-total_epoch_loss_test)
            print("[epoch %03d] average test loss: %.4f" % (epoch, total_epoch_loss_test))
        save_checkpoint(vae, 'vae.pth')
    import matplotlib.pyplot as plt
    plt.plot(train_elbo);plt.show()
    plt.plot(range(0, NUM_EPOCHS, TEST_FREQUENCY), test_elbo);plt.show()  
    load_checkpoint(vae, 'vae.pth')


if __name__ == '__main__':
    run()

'''
Created on Oct 8, 2020

@author: ec2-user
'''
from config import KAGGLE_INPUT_HOME
import os, sys 
import pandas as pd 
import numpy as np 
from sklearn.decomposition import PCA


class Data:

    def __init__(self, remove_ctl_vehicle=True, normalize=True, use_pca=True):
        self.remove_ctl_vehicle = remove_ctl_vehicle
        self.normalize = normalize
        self.use_pca = use_pca
    
    @property 
    def g_columns(self):
        return ['g-{}'.format(i) for i in range(772)]
    
    def _cat_mapping(self, df):
        mapping = {"cp_type":{"trt_cp": 0, "ctl_vehicle":1},
               "cp_time":{48:0, 72:1, 24:2},
               "cp_dose":{"D1":0, "D2":1}}
        for col, d in mapping.items():
            if col in df.columns:
                for k, _ in d.items():
                    df['{}_{}'.format(col, k)] = (df[col] == k).astype(np.uint8)
                df.drop(col, axis=1, inplace=True)
        return df 
    
    def _normalize(self, df):
        max_ = 10.
        min_ = -10.
        for col in self.g_columns + self.c_columns:
            df[col] = (df[col] - min_) / (max_ - min_)
        return df
    
    def get_pcas(self):
        if not hasattr(self, '_pcas'):
            n_comp = 28
            data = pd.concat([self.train_feat_df[self.g_columns], self.test_feat_df[self.g_columns]])
            pca1 = PCA(n_components=n_comp).fit(data.values)
            n_comp = 5
            data = pd.concat([self.train_feat_df[self.c_columns], self.test_feat_df[self.c_columns]])
            pca2 = PCA(n_components=n_comp).fit(data.values)
            self._pcas = pca1, pca2
        return self._pcas

    @property 
    def c_columns(self):
        return ['c-{}'.format(i) for i in range(100)]
    
    def _pca_feats(self, df):
        gpca, cpca = self.get_pcas()
        a = gpca.transform(df[self.g_columns].values)
        gdf = pd.DataFrame(a, index=df.index, columns=[f'pca_g-{i}' for i in range(a.shape[1])])
        a = cpca.transform(df[self.c_columns].values)
        cdf = pd.DataFrame(a, index=df.index, columns=[f'pca_c-{i}' for i in range(a.shape[1])])
        return pd.concat([gdf, cdf], axis=1)
        
    @property 
    def train_pca(self):
        return self._pca_feats(self.train_feat_df)

    @property 
    def test_pca(self):
        return self._pca_feats(self.test_feat_df)

    @property
    def train(self):
        if not hasattr(self, '_train'):
            df = self.train_feat_df
            if self.use_pca:
                df = pd.concat([df, self.train_pca], axis=1)
            self._train = df
        return self._train

    @property
    def test(self):
        if not hasattr(self, '_test'):
            df = self.test_feat_df
            if self.use_pca:
                df = pd.concat([df, self.test_pca], axis=1)
            self._test = df
        return self._test
            
    @property 
    def train_feat_df(self):
        if not hasattr(self, '_train_feat_df'):
            df = pd.read_csv(os.path.join(KAGGLE_INPUT_HOME, 'train_features.csv'), index_col=0)
            if self.remove_ctl_vehicle:
                df = df[df['cp_type'] != 'ctl_vehicle'].drop('cp_type', axis=1)
            if self.normalize:
                df = self._normalize(df)
            df = self._cat_mapping(df)
            self._train_feat_df = df.astype(np.float32)
        return self._train_feat_df

    @property
    def train_target_df(self):
        if not hasattr(self, '_train_target_df'):
            self._train_target_df = pd.read_csv(os.path.join(KAGGLE_INPUT_HOME, 'train_targets_scored.csv'),
                                                index_col=0).loc[self.train_feat_df.index].astype(np.uint8)
            
        return self._train_target_df
    
    @property
    def train_targets_nonscored_df(self):
        if not hasattr(self, '_train_targets_nonscored_df'):
            df = pd.read_csv(os.path.join(KAGGLE_INPUT_HOME, 'train_targets_nonscored.csv'), index_col=0).loc[self.train_feat_df.index]
            cols = df.columns [df.sum(axis=0) != df.shape[0]]
            self._train_targets_nonscored_df = df[cols].astype(np.uint8)
        return self._train_targets_nonscored_df
    
    @property 
    def test_feat_df(self):
        if not hasattr(self, '_test_feat_df'):
            df = pd.read_csv(os.path.join(KAGGLE_INPUT_HOME, 'test_features.csv'), index_col=0)
            if self.remove_ctl_vehicle:
                df = df[df['cp_type'] != 'ctl_vehicle'].drop('cp_type', axis=1)
            if self.normalize:
                df = self._normalize(df)
            df = self._cat_mapping(df)
            self._test_feat_df = df.astype(np.float32)
                    
        return self._test_feat_df
    
    @property
    def sample_submission(self):
        if not hasattr(self, '_sample_submission'):
            self._sample_submission = (pd.read_csv(os.path.join(KAGGLE_INPUT_HOME, 'sample_submission.csv'), index_col=0) * 0).astype(np.float32)
        return self._sample_submission


if __name__ == '__main__':
    data = Data() 
    print(data.train_feat_df.head())
    print(data.train_target_df.head())
    print(data.train_targets_nonscored_df.head())
    print(data.test_feat_df.head())
    print(data.sample_submission.head())
    
    assert data.train_feat_df.shape[0] == data.train_target_df.shape[0]
    assert data.train_feat_df.shape[0] == data.train_targets_nonscored_df.shape[0]
    
    assert data.train_feat_df.shape[1] == data.test_feat_df.shape[1]
    assert data.train_target_df.shape[1] == data.sample_submission.shape[1]
    
    assert (data.train_feat_df.index == data.train_target_df.index).all()
    assert (data.train_feat_df.index == data.train_targets_nonscored_df.index).all()
    
    print(data.train_pca.shape, data.test_pca.shape)
    print(len(data.g_columns), len(data.c_columns))
    print(data.train_feat_df.min().min(), data.train_feat_df.max().max())

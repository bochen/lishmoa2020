'''
Created on Aug 16, 2020

@author: bo
'''

import pickle 
import gzip
import os,sys
import random
import torch
import numpy as np
try: 
    import pyro
except:
    pass 

def pickle_dumpz(obj, filepath):
    with gzip.open(filepath, 'w') as f:
        pickle.dump(obj, f)

        
def pickle_loadz(filepath):
    with gzip.open(filepath, 'r') as f:
        return pickle.load(f)        

def create_folder_if_not_exists(path):
    if not os.path.exists(path):
        os.mkdir(path)

def seed_everything(seed=1903):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    try:
        pyro.set_rng_seed(seed)
    except:
        pass 
    

def log_loss_score(actual, predicted,  eps=1e-15):

        """
        :param predicted:   The predicted probabilities as floats between 0-1
        :param actual:      The binary labels. Either 0 or 1.
        :param eps:         Log(0) is equal to infinity, so we need to offset our predicted values slightly by eps from 0 or 1
        :return:            The logarithmic loss between between the predicted probability assigned to the possible outcomes for item i, and the actual outcome.
        """

        
        p1 = actual * np.log(predicted+eps)
        p0 = (1-actual) * np.log(1-predicted+eps)
        loss = p0 + p1

        return -loss.mean()
    
def log_loss_multi(y_true, y_pred):
    M = y_true.shape[1]
    results = np.zeros(M)
    for i in range(M):
        results[i] = log_loss_score(y_true[:,i], y_pred[:,i])
    return results.mean()

    
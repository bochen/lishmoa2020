'''
Created on Aug 13, 2020

@author: bo
'''

import os 

if 'KAGGLE_ENV' in os.environ:
    KAGGLE_HOME = "/kaggle/input/"
    KAGGLE_INPUT_HOME = os.path.join(KAGGLE_HOME, "lish-moa")
    KAGGLE_SANDBOX_HOME = "/tmp"
else:
    KAGGLE_HOME = os.path.join(os.environ['HOME'], 'mydev', 'lishmoa2020')
    KAGGLE_INPUT_HOME = os.path.join(KAGGLE_HOME, 'input')
    KAGGLE_SANDBOX_HOME = os.path.join(KAGGLE_HOME, 'sandbox')


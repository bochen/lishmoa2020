'''
Created on Oct 10, 2020

@author: ec2-user
'''

import os

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F

from data import Data
import utils
from tqdm import tqdm 
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class Option:
    pass 


Option.n_layer = 1
Option.n_input_features = 872


class MoAVAEDataset:

    def __init__(self, features):
        self.features = features

    def __len__(self):
        return (self.features.shape[0])
    
    def __getitem__(self, idx):
        x = torch.tensor(self.features[idx, :], dtype=torch.float)
        dct = {
            'x' : x,
        }
        return dct


def setup_data_loaders(batch_size=128, use_cuda=False, use_all_data=False):
    data = Data(use_pca=False)
    columns = data.g_columns + data.c_columns
    test_ = data.test[columns]
    train_ = data.train[columns]
    train_test = pd.concat([train_, test_], axis=0)
    train_test = train_test.loc[np.random.permutation(train_test.index)]
    print('min/max', train_test.min().min(), train_test.max().max())
    k = train_test.shape[0] // 5
    if use_all_data:
        train_df = train_test
    else:
        train_df = train_test.iloc[k:]
    valid_df = train_test.iloc[:k]
    
    print('min mse loss', loss_function2(train_df.values, train_df.values))
    print('train.shape={}, valid.shape={}'.format(train_df.shape, valid_df.shape))
    train_set = MoAVAEDataset(train_df.values)
    valid_set = MoAVAEDataset(valid_df.values)
    
    kwargs = {'num_workers': 1, 'pin_memory': use_cuda}
    train_loader = torch.utils.data.DataLoader(dataset=train_set,
        batch_size=batch_size, shuffle=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(dataset=valid_set,
        batch_size=batch_size, shuffle=False, **kwargs)
    return train_loader, test_loader


def loss_function(recon_x, x, mu, logvar):
    
    b_z = x.shape[0]
    CE = F.binary_cross_entropy(recon_x, x, reduction='sum') / b_z
    a = torch.abs(recon_x - x)
    MSE = torch.sum(a * a) / b_z
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp()) / b_z
    return CE + KLD, CE, KLD, MSE

  
def loss_function2(recon_x, x):

    def g(yt, yp):
        return -(yt * np.log(yp) + (1 - yt) * np.log(1 - yp))
    
    n = x.shape[0]
    recon_x = recon_x.copy().astype(np.float64)
    eps = 1e-15
    recon_x[recon_x < eps] = eps
    recon_x[recon_x > 1 - eps] = 1 - eps
    return np.sum(g(x, recon_x)) / n


class Decoder(nn.Module):

    def __init__(self, z_dim, hidden_dim):
        super().__init__()
        if 1:
            self.fc1 = nn.Linear(z_dim, hidden_dim)
            self.bn1 = nn.BatchNorm1d(hidden_dim, momentum=0.01)
            self.softplus1 = nn.Softplus()
            self.fc2 = nn.Linear(hidden_dim, hidden_dim)
            self.bn2 = nn.BatchNorm1d(hidden_dim, momentum=0.01)
            self.softplus2 = nn.Softplus()
            self.fco1 = nn.Linear(hidden_dim, Option.n_input_features)
            self.sigmod = nn.Sigmoid()  # y = (y1, y2, y3) \in [0 ,1]^3
# setup the non-linearities

    def forward(self, z):
        hidden = self.softplus1(self.bn1(self.fc1(z)))
        if Option.n_layer >= 2:
            hidden = self.softplus2(self.bn2(self.fc2(hidden)))
        loc_img = self.fco1(hidden)
        loc_img = self.sigmod(loc_img)
        return loc_img
 

class Encoder(nn.Module):

    def __init__(self, z_dim, hidden_dim):
        super().__init__()
        # setup the three linear transformations used
        if 1:
            self.fc1 = nn.Linear(Option.n_input_features, hidden_dim)
            self.bn1 = nn.BatchNorm1d(hidden_dim, momentum=0.01)
            self.softplus1 = nn.Softplus()
            self.fc2 = nn.Linear(hidden_dim, hidden_dim)
            self.bn2 = nn.BatchNorm1d(hidden_dim, momentum=0.01)
            self.softplus2 = nn.Softplus()
            self.fco1 = nn.Linear(hidden_dim, z_dim)
            self.fco2 = nn.Linear(hidden_dim, z_dim)            

    def forward(self, x):
        x = self.softplus1(self.bn1(self.fc1(x)))
        if Option.n_layer >= 2:
            x = self.softplus2(self.bn2(self.fc2(x)))
        
        hidden = x  # each of size batch_size x z_dim
        z_loc = self.fco1(hidden)
        z_logvar = self.fco2(hidden)
        return z_loc, z_logvar


class VAE(nn.Module):

    # by default our latent space is 50-dimensional
    # and we use 400 hidden units
    def __init__(self, z_dim=128, hidden_dim=512, use_cuda=False):
        super().__init__()
        # create the encoder and decoder networks
        self.encoder = Encoder(z_dim, hidden_dim)
        self.decoder = Decoder(z_dim, hidden_dim)

        if use_cuda:
            self.cuda()
        self.use_cuda = use_cuda
        self.z_dim = z_dim

    def reparameterize(self, mu, logvar):
        if self.training:
            std = logvar.mul(0.5).exp_()
            eps = Variable(std.data.new(std.size()).normal_())
            return eps.mul(std).add_(mu)
        else:
            return mu

            # define the model p(x|z)p(z)
    # define a helper function for reconstructing images
    def forward(self, x):
        mu, logvar = self.encoder(x)
        z = self.reparameterize(mu, logvar)
        x_reconst = self.decoder(z)
        return x_reconst, z, mu, logvar 


def float_to_str(x):
    return '{:.4f}'.format(x)

    
def train(model, train_loader, optimizer, epoch, use_cuda=False):
    # initialize loss accumulator
    model.train()

    total_loss = 0
    total_mse = 0 
    total_kld = 0
    total_ce = 0
    step = 0
    t = tqdm(enumerate(train_loader))
    t.set_description('train epoch: ' + str(epoch))
    for batch_idx, batch in t:
        X = batch["x"]
        if use_cuda: 
            X = X.cuda()

        optimizer.zero_grad()
        X_reconst, z, mu, logvar = model(X)  # VAE
        loss, ce, kld, mse = loss_function(X_reconst, X, mu, logvar)
        total_loss += (loss.item())
        total_mse += mse.item() 
        total_kld += kld.item() 
        total_ce += ce.item()
        # total_bce += bce.item()

        loss.backward()
        optimizer.step()
        
        step += 1
        d = dict(total_loss=total_loss / step, mse=total_mse / step, kld=total_kld / step, ce=total_ce / step)
        t.set_postfix({k:float_to_str(v) for k, v in d.items()})
    return d


def validation(model, test_loader, optimizer, epoch, use_cuda=False):

    model.eval()

    total_loss = 0
    total_mse = 0 
    total_ce = 0
    total_kld = 0
    step = 0
    with torch.no_grad():
        t = tqdm(enumerate(test_loader))
        t.set_description('valid epoch: ' + str(epoch))
        for _, batch in t:
            X = batch["x"]
            if use_cuda: X = X.cuda()  # distribute data to device
            X_reconst, z, mu, logvar = model(X)

            loss, ce, kld, mse = loss_function(X_reconst, X, mu, logvar)
            total_loss += (loss.item())
            total_mse += mse.item()  
            total_ce += ce.item()
            total_kld += kld.item() 
            step += 1
            d = dict(total_loss=total_loss / step, mse=total_mse / step, kld=total_kld / step, ce=total_ce / step)
            t.set_postfix({k:float_to_str(v) for k, v in d.items()})

    return d 


# saves the model and optimizer states to disk
def save_checkpoint(model, filepath):
    torch.save(model.state_dict(), filepath)


# loads the model and optimizer states from disk
def load_checkpoint(model, filepath):
    assert os.path.exists(filepath)
    sd = torch.load(filepath)
    model.load_state_dict(sd)

        
def run():
    
    utils.seed_everything(123)
    
    USE_CUDA = False
    
    # Run only for a single iteration for testing
    NUM_EPOCHS = 150
    
    train_loader, test_loader = setup_data_loaders(batch_size=256, use_cuda=USE_CUDA)
    # setup the VAE
    vae = VAE(use_cuda=USE_CUDA)

    model_params = list(vae.parameters())
    optimizer = torch.optim.Adam(model_params)
    
    start_epoch = 0
    train_losses = []
    valid_losses = []
    for epoch in range(start_epoch, NUM_EPOCHS):
        a = train(vae, train_loader, optimizer, epoch, use_cuda=False)
        train_losses.append(a)
        a = validation(vae, test_loader, optimizer, epoch, use_cuda=False)
        valid_losses.append(a)
        save_checkpoint(vae, 'vae2.pth')

    train_losses = pd.DataFrame(train_losses)
    valid_losses = pd.DataFrame(valid_losses)
    import matplotlib.pyplot as plt
    plt.plot(train_losses['total_loss'])
    plt.plot(valid_losses['total_loss'])
    plt.title("ce+kld")
    plt.show()
    plt.plot(train_losses['ce'])
    plt.plot(valid_losses['ce'])
    plt.title('ce')
    plt.show()    
    plt.plot(train_losses['mse'])
    plt.plot(valid_losses['mse'])
    plt.title("mse")
    plt.show()
    plt.plot(train_losses['kld'])
    plt.plot(valid_losses['kld'])
    plt.title('kld')
    plt.show()
    load_checkpoint(vae, 'vae2.pth')


if __name__ == '__main__':
    run()
